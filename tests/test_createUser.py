import unittest
from website import auth


class TestCreateUser(unittest.TestCase):
    def test_with_valid_password(self):
        username = "testuser"
        password = "Password1!" # valid
        repassword = "Password1!"
        user = None
        self.assertEqual(auth.validate_user(username, password, repassword, user), True)

    def test_with_invalid_regex(self):
        username = "testuser"
        password = "password"  # no uppercase, number or special character
        repassword = "password"
        user = None
        self.assertEqual(auth.validate_user(username, password, repassword, user), "Password must contain 1 Upper, 1 lower, 1 number, 1 special character, 8 characters" )

    def test_with_invalid_username(self):
        username = "123" # too short
        password = "Password1!"
        repassword = "Password1!"
        user = None
        self.assertEqual(auth.validate_user(username, password, repassword, user), "Username must be at least 5 characters.")

    def test_with_invalid_username_already_exists(self):
        username = "testuser"
        password = "Password1!"
        repassword = "Password1!"
        user = "testuser"
        self.assertEqual(auth.validate_user(username, password, repassword, user), "Username already exists.")

if __name__ == '__main__':
    unittest.main()