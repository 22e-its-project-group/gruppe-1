from flask import Blueprint, render_template, request, flash, redirect, url_for, render_template_string
from flask_login import login_required, current_user
from .models import Img, Comment, Share, User, Entry
from . import db, ALLOWED_EXTENSIONS, UPLOAD_FOLDER
import os
import logging
from werkzeug.utils import secure_filename


views = Blueprint('views', __name__)


@views.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html', user=current_user)


@views.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    return render_template('profile.html', user=current_user, )


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@views.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    if request.method == 'POST':
        file = request.files['file']
        if not file:
            flash('No Picture uploaded', category='error')
        picCount = Img.query.filter_by(user_id=current_user.id).count()

        #insecure filename
        filename = file.filename

        #Secure filename
        #filename = secure_filename(file.filename)

        # Users can't upload more than 100 images
        if picCount >= 99:
            logging.error(f"User: {current_user.username} IP: {request.remote_addr} has reached the max number of images")
            return 'Upload limit reached!', 400

        if not filename: #or not allowed_file(filename):
            logging.error(f"User: {current_user.username} IP: {request.remote_addr} tried to upload a file with an invalid extension")
            return 'Bad upload!', 400

        image = Img(filename=filename, user_id=current_user.id)
        db.session.add(image)
        db.session.commit()
        file.save(os.path.join(UPLOAD_FOLDER, filename))
        flash('Picture uploaded : %s' % filename, category='Success')
    return render_template("upload.html", user=current_user)


@views.route('/showimage/<id>/', methods=['GET'])
@login_required
def show_image(id):
    usr = User.query.all()
    img = Img.query.filter_by(id=id).first()
    if not img:
        #return 'Img Not Found: ' + id, 404
        #vulnerable to SSTI
        return render_template_string('Img Not Found: ' + id), 404
    return render_template("image.html", usernames=usr, user=current_user, img=img, owner=img.user_id == current_user.id)


@views.route('/add_comment', methods=['POST'])
@login_required
def add_comment():
    if request.method == 'POST':
        image_id = request.form['imageid']
        comment = request.form['text']

        commentCount = Comment.query.filter_by(image_id=image_id).count()

        # Images can't have more than 100 comments
        if commentCount >= 99:
            return 'Comment limit reached!', 400

        new_comment = Comment(data=comment, user_id=current_user.id, image_id=image_id)
        db.session.add(new_comment)
        db.session.commit()
        flash('Comment added!', category='success')

        return redirect(url_for('views.show_image', id=image_id))


@views.route('/shareimage', methods=['POST'])
@login_required
def share_image():
    if request.method == 'POST':
        to_user = request.form.get('userid')
        imgId = request.form.get('imgId')
        shared_filename = db.session.query(Img.filename).filter(Img.id == imgId)
        new_share = Share(image_id=imgId, from_user=current_user.id, to_user=to_user, filename=shared_filename)
        db.session.add(new_share)
        db.session.commit()
        flash('Image has been shared!', category='success')
        return redirect(url_for('views.profile'))


@views.route('/unshare', methods=['POST'])
@login_required
def unshare():
    if request.method == 'POST':
        shared_id = request.form['shareduser']
        image_id = request.form['imageid']

        Share.query.filter(Share.to_user == shared_id and Share.image_id == image_id).delete()
        flash('Image unshared',)
        return redirect(url_for('views.profile'))
    else:
        return redirect(url_for('views.no_way'))


@views.route('/no_way', methods=['GET'])
def no_way():
    return render_template('no_way.html')


@views.route('/show_entries', methods=['GET'])
def show_entries():
    entries = Entry.query.all()
    return render_template('show_entries.html', user=current_user, entries=entries)


@views.route('/add', methods=['POST']) #add entries
@login_required
def add_entry():
    if request.method == 'POST':
        text = request.form['text']
        title = request.form['title']
        new_entry = Entry(title=title, text=text)
        db.session.add(new_entry)
        db.session.commit()
        flash('New entry was successfully posted')
    return redirect(url_for('views.show_entries'))


